# reMarkable Splash screens Utility

This script helps copy splash screens to your reMarkable tablet. You can keep your collection of old splashscreens in the same directory and just swap in the one you want to use at the time. 

It is a simple utility, allowing you to quickly upload all splashscreens to the remarkable. 

> **info**
>
> The splash.bmp (splashscreen when booting) is not supported, as this requires some extra steps.

### Usage

You simply copy the `.png` files to the `selected` directory in the respective category directory (e.g. ` suspended`). You can store backups or different versions (if you like switching between multiple versions) in the  category directory. 

The file in the `selected` directory will be uploaded to the remarkable. It will automatically be renamed when it is copied over.

In the example below `cool_image.png` will be copied over as suspended.png to the remarkable. The other images are ignored.

```
suspended/
├─ cool_image.png
├─ for_christmas_only.png
├─ this_one_is_nice_too.png
├─ old_splashscreen_that_I_may_use_again.png
├─ selected/
│  ├─ cool_image.png
```
To copy over the files to the remarkable, you simply run the python script (assuming you have gone through the setup).

```bash
python splashscreens_to_remarkable.py
```

Which you will probably want to alias to something nicer on your system (see setup).

### Setup

The setup assumes you are running a linux system. If not, you can use WSL for windows and run everything through that. Everything is tested on WSL on a windows 10 system.

We will need to set up the following things

1. Set up [SSH key for the remarkable](https://remarkablewiki.com/tech/ssh#Tips)
2. Set up an [SSH alias for the remarkable](https://remarkablewiki.com/tech/ssh#Tips) (i.e. so we can connect to remarkable via e.g. `ssh remarkable`)
   1. ⚠️If you chose a different alias than `remarkable`, you will need to change the `ssh_alias` in [the script file](splashscreens_to_remarkable.py)
   2. It is probably easiest to use the wired address (`10.11.99.1`) that is available when connected via USB.
3. Clone this repository somewhere on your system
4. Ensure you have a python3 installation 
5. Add some splashscreens to the folders so they can be copied over
6. Add an alias to your system/WSL to copy over files. 
   7. go to home directory `cd`
   2. open bashrc (or equivalent) `nano .bashrc`
   3. Add the lines `alias rMimgs='python3 absolute/path/to/cloned/repository/splashscreens_to_remarkable.py  ` at the end of the file. You can choose a different alias name if you prefer. *Depending on your system you may have to replace `python3` with `python`.*

> **info**
>
> If you have followed the above, you can copy your splashscreens to the remarkable by (1) connecting the reMarkable via USB, (2) Opening terminal/WSL (3) Typing `rMimgs` (or whatever alias you set).

### On making splashscreens

The splashscreens must be `.png` format (with one exception). They must also be a specific size (1404x1872 - for reMarkable 2).



 

