#!/usr/bin/env python3

import os
import pathlib
import warnings
import subprocess

# Note: we assume you have an SSH alias for the remarkable listed as  'remarkable'
# and that you have an SSH key set up so no password is needed
ssh_alias = 'remarkable'  # change if you have something else (e.g. 'rM' or "192.168.1.20")

# get path of the script file itself (not necessarily current working directory)
splashscreens_dir = pathlib.Path(__file__).parent.resolve()
print(splashscreens_dir)
print(os.listdir(splashscreens_dir))

# list of subdirectories such as 'poweroff' and 'suspended'
subdirectories = list(filter(lambda x: os.path.isdir(os.path.join(splashscreens_dir,x)),
 os.listdir(splashscreens_dir)))
print(subdirectories)

transferred = []    
ignored = []

for directory in subdirectories:    
    # get the name of whatever png might be in the 'selected' directory
    selected_contents = os.listdir(os.path.join(splashscreens_dir, directory, "selected"))
    existing_img = list(filter(lambda x: x.endswith((".png", ".PNG")), selected_contents))
    if not existing_img:
        warnings.warn(f"- No image selected for '{directory}', skipping...")
        ignored.append(directory)
    else:
        # rename the file in the 'current' directory to match the name of directory+png
        output_path = os.path.join(splashscreens_dir, directory, "selected", directory + ".png")
        os.rename(
            os.path.join(splashscreens_dir, directory, "selected", existing_img[0]),
            output_path)

        # SFTP copy file to remarkable
        print(f'- copying "{directory}.png" splashscreen to remarkable...')   
        try:
            subprocess.run(['sftp', ssh_alias+":/usr/share/remarkable"],
             input=str.encode("put "+ output_path), check=True) 

            transferred.append(directory)  # so we can print a summary
        except subprocess.CalledProcessError:
            print(f'SFTP error. Unable to copy "{directory}" splashscreen to rM.')

# print a summary
print("> Finished copying to remarkable")
if transferred:
    print(f"copied the following {len(transferred)} files succesfully")
    for file in transferred:
        print(f" - {file}")
else:
    print("No splashscreens were transferred. Did you make sure there are files in "
     "the 'selected' directories?")
    
if ignored:
    print(f"ignored the following {len(ignored)} files (not present)")
    for file in ignored:
        print(f" - {file}")
else:
    print("No files were ignored (all splashscreens present)")





